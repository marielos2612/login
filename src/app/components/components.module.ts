import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { SlidesComponent } from '../components/slides/slides.component';
import { StartComponent } from './start/start.component';
import { LogoComponent } from '../components/logo/logo.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [SlidesComponent,StartComponent,LogoComponent],
  exports: [SlidesComponent,StartComponent,LogoComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ]
})
export class ComponentsModule { }
