const mongoose = require('mongoose');

/**
 * Validation
 */
/*function validateLength (v) {
  // a custom validation function for checking string length to be used by the model
  return v.length <= 15;
}
*/

/*
 * Tank Schema
 */
const sensorSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  mac: String,
  system: String,
  client:{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Client"
  },
  xpos: Number,
  ypos: Number
});

mongoose.model('Sensor', sensorSchema);