const mongoose = require('mongoose');

/**
 * Validation
 */
 /*
function validateLength (v) {
  // a custom validation function for checking string length to be used by the model
  return v.length <= 15;
}*/

/*
 * Tanks System Schema
 */
const monitoruserLogSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  user:{
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  monitortank:{
        type: mongoose.Schema.Types.ObjectId,
        ref: "MonitorTank"
  },
  info:{
    type: String,
    required: true
  },
  createdDate: { type: Date, default: Date.now }
});

mongoose.model('MonitorUserLog', monitoruserLogSchema);