const mongoose = require('mongoose');

/**
 * Validation
 */
 /*
function validateLength (v) {
  // a custom validation function for checking string length to be used by the model
  return v.length <= 15;
}*/

/*
 * Tanks System Schema
 */
const tanksSystemSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  client:{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Client"
  },
  tanks: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Tank"
      }
    ],
   temp_cpu: Number,
   disk_size: Number, 
   disk_used: Number, 
   disk_free: Number,
   humedad: Number
});

mongoose.model('TanksSystem', tanksSystemSchema);