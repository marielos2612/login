const mongoose = require("mongoose");

const roleSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true
  },
  createdDate: { type: Date, default: Date.now }
});

mongoose.model('Role', roleSchema);
