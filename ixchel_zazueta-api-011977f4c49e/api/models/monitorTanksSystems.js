const mongoose = require('mongoose');

/**
 * Validation
 */
 /*
function validateLength (v) {
  // a custom validation function for checking string length to be used by the model
  return v.length <= 15;
}*/

/*
 * Tanks System Schema
 */
const monitortanksSystemSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  client:{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Client"
  },
  monitortanks: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "MonitorTank"
      }
    ],
   temp_cpu: Number,
   disk_size: Number, 
   disk_used: Number, 
   disk_free: Number,
   humedad: Number
});

mongoose.model('MonitorTanksSystem', monitortanksSystemSchema);