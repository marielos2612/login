const mongoose = require('mongoose');

/**
 * Validation
 */
/*function validateLength (v) {
  // a custom validation function for checking string length to be used by the model
  return v.length <= 15;
}
*/

/*
 * Tank Schema
 */
const sensorDataSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  humidity: Number,
  lux: Number,
  rssi: Number,
  batt: Number,
  bar: Number,
  sensor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Sensor"
  },
  createdDate: { type: Date, default: Date.now },
  temp: {
    type: Number,
    required: true
  }
});

mongoose.model('SensorData', sensorDataSchema);