const mongoose = require('mongoose');

/**
 * Validation
 */
/*function validateLength (v) {
  // a custom validation function for checking string length to be used by the model
  return v.length <= 15;
}
*/

/*
 * Tank Schema
 */
const wineryBarrelStorageSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  sensors: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Sensor"
      }
    ],
  observations: String,
  client:{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Client"
  },
  batt: String
});

mongoose.model('WineryBarrelStorage', wineryBarrelStorageSchema);