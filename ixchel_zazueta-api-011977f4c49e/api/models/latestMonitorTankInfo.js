const mongoose = require('mongoose');

/**
 * Validation
 */
 /*
function validateLength (v) {
  // a custom validation function for checking string length to be used by the model
  return v.length <= 15;
}*/

/*
 * Tank Info per Minute Schema
 */
const latestMonitorTankInfoSchema = new mongoose.Schema({
  monitortank: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "MonitorTank"
  },
  temp: {
    type: Number,
    required: true
  },
  alarm: Boolean,
  zone: Boolean,
  createdDate: { type: Date, default: Date.now }
});

/*
{"Temp":16.375,"Cliente":"alximia","Tanque_ID":1,"Mac":"FCF5C4547948","Count":66117,
"Relay":false,"Control_enable":true,"Setpoint":17,"Tmin":16,"Alarm":false,"Zone":true}
*/

mongoose.model('LatestMonitorTankInfo', latestMonitorTankInfoSchema);