const mongoose = require('mongoose');

/**
 * Validation
 */
 /*
function validateLength (v) {
  // a custom validation function for checking string length to be used by the model
  return v.length <= 15;
}*/

/*
 * Tanks System Schema
 */
const userLogSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  user:{
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  tank:{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Tank"
  },
  info:{
    type: String,
    required: true
  },
  createdDate: { type: Date, default: Date.now }
});

mongoose.model('UserLog', userLogSchema);