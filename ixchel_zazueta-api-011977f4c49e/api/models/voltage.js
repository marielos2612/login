const mongoose = require('mongoose');

/**
 * Validation
 */
 /*
function validateLength (v) {
  // a custom validation function for checking string length to be used by the model
  return v.length <= 15;
}*/

/*
 * Voltage Schema
 */
const voltageSchema = new mongoose.Schema({
  client: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Client"
  },
  volt1: {
    type: Number,
    required: true
  },
  volt2: {
    type: Number,
    required: true
  },
  volt3: {
    type: Number,
    required: true
  },
  timestamp: Number,
  timestampDate: { type: Date },
  alarm: Boolean,
  createdDate: { type: Date, default: Date.now }
});

/*
{"Temp":16.375,"Cliente":"alximia","Tanque_ID":1,"Mac":"FCF5C4547948","Count":66117,
"Relay":false,"Control_enable":true,"Setpoint":17,"Tmin":16,"Alarm":false,"Zone":true}
*/

mongoose.model('Voltage', voltageSchema);