require('./roles');
require('./client');
require('./users');
require('./tanks');
require('./userLogs');
require('./tanksSystems');
require('./tankInfoPerFiveSeconds');
require('./voltage');
require('./environment');
require('./sensor');
require('./wineryBarrelStorage');
require('./sensorData');
require('./latestTankInfo');
require('./monitorTanks');
require('./monitorTanksSystems');
require('./monitorTankInfoPerFiveSeconds');
require('./latestMonitorTankInfo');
require('./monitorUserLogs');

const mongoose = require('mongoose');
const dbURI = 'mongodb://localhost:27017/meanAuth';

const Role = mongoose.model('Role');
const WineryBarrelStorage = mongoose.model('WineryBarrelStorage');

mongoose.set('useCreateIndex', true);
mongoose.connect(dbURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
    initial();
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });

mongoose.connection.on('connected', () => {
  console.log(`Mongoose connected to ${dbURI}`);
});
mongoose.connection.on('error', (err) => {
  console.log(`Mongoose connection error: ${err}`);
});
mongoose.connection.on('disconnected', () => {
  console.log('Mongoose disconnected');
});

function initial() {
  Role.estimatedDocumentCount((err, count) => {
    if (!err && count === 3) {
      /*new Role({
        name: "user"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'user' to roles collection");
      });

      new Role({
        name: "operator"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'moderator' to roles collection");
      });

      new Role({
        name: "admin"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'admin' to roles collection");
      });*/

      new Role({
        name: "admin_tanks"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'admin_tanks' to roles collection");
      });

      new Role({
        name: "user_tanks"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'user_tanks' to roles collection");
      });

      new Role({
        name: "admin_tanks_nocontrol"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'admin_tanks_nocontrol' to roles collection");
      });

      new Role({
        name: "user_tanks_nocontrol"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'user_tanks_nocontrol' to roles collection");
      });

      new Role({
        name: "admin_cava"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'admin_cava' to roles collection");
      });

      new Role({
        name: "user_cava"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'user_cava' to roles collection");
      });

      new Role({
        name: "admin_voltage"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'admin_voltage' to roles collection");
      });

      new Role({
        name: "user_voltage"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'user_voltage' to roles collection");
      });

      new Role({
        name: "super_admin"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'super_admin' to roles collection");
      });


    }
  });

}