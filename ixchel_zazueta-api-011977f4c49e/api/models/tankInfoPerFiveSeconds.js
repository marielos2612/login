const mongoose = require('mongoose');

/**
 * Validation
 */
 /*
function validateLength (v) {
  // a custom validation function for checking string length to be used by the model
  return v.length <= 15;
}*/

/*
 * Tank Info per Minute Schema
 */
const tanksSystemSchema = new mongoose.Schema({
  tank: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Tank"
  },
  temp: {
    type: Number,
    required: true
  },
  controlEnabled: Boolean,
  valveStatus: Boolean,
  maxTemp: Number,
  minTemp: Number,
  alarm: Boolean,
  zone: Boolean,
  createdDate: { type: Date, default: Date.now }
});

/*
{"Temp":16.375,"Cliente":"alximia","Tanque_ID":1,"Mac":"FCF5C4547948","Count":66117,
"Relay":false,"Control_enable":true,"Setpoint":17,"Tmin":16,"Alarm":false,"Zone":true}
*/

mongoose.model('TankInfoPerFiveSeconds', tanksSystemSchema);