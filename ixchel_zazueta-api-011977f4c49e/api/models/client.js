const mongoose = require('mongoose');

/*
 * Client Schema
 */
const clientSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  users: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
      }
    ],
  imageUrl: String,
  wineryImageUrl: String,
  createdDate: { type: Date, default: Date.now }
});

mongoose.model('Client', clientSchema);