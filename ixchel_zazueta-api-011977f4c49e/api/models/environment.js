const mongoose = require('mongoose');

/**
 * Validation
 */
/*function validateLength (v) {
  // a custom validation function for checking string length to be used by the model
  return v.length <= 15;
}
*/

/*
 * Tank Schema
 */
const environmentSchema = new mongoose.Schema({
  temp: Number,
  disk_size: Number, 
  disk_used: Number, 
  disk_free: Number,
  humedad: Number,
  createdDate: { type: Date, default: Date.now },
  client:{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Client"
  }
});

mongoose.model('Environment', environmentSchema);