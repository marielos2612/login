const mongoose = require('mongoose');

/**
 * Validation
 */
/*function validateLength (v) {
  // a custom validation function for checking string length to be used by the model
  return v.length <= 15;
}
*/

/*
 * Tank Schema
 */
const monitortankSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  volume: String,
  varietal: String,
  type: String,
  observations: String,
  client:{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Client"
  },
  isEmpty: Boolean,
  maxTemp: Number,
  minTemp: Number
});

mongoose.model('MonitorTank', monitortankSchema);