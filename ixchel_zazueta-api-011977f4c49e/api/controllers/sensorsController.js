const mongoose = require('mongoose');
const Sensor = mongoose.model('Sensor');
const ObjectId = mongoose.Types.ObjectId;

//function getAll(req, res, next)
module.exports.getAll = (req, res, next) => {

  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_cava" || entry["name"] === "user_cava" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
      Sensor.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
  }
}

//function getCurrent(req, res, next) 
/*module.exports.getCurrent = (req, res, next) => {
    User.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}*/

//function getById(req, res, next) {
module.exports.getById = (req, res, next) => {
  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_cava" || entry["name"] === "user_cava" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
    Sensor.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
  }
}

//function update(req, res, next) {
module.exports.update = (req, res, next) => {
  //console.log(req.params.id);
  //console.log(req.body);
  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_cava" || entry["name"] === "user_cava" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
    Sensor.updateOne({ _id : req.params.id}, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
  }
}


//function _delete(req, res, next) {
module.exports._delete = (req, res, next) => {
  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_cava" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
    Sensor.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
  }
}


module.exports.register = (req, res) => {

  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_cava" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
    const tank = new Sensor();

    if(req.body.name && req.body.client){

      tank.name = req.body.name;
      if(req.body.mac){tank.mac = req.body.mac;}
      if(req.body.system){tank.system = req.body.system; }
      tank.client = req.body.client;

      /*
        Don’t forget that, in reality, this code would have a number of error traps, 
        validating form inputs and catching errors in the save function. 
        They’re omitted here to highlight the main functionality of the code, 
        but if you’d like a refresher, check out “Forms, File Uploads and Security with Node.js and Express”.
        https://www.sitepoint.com/forms-file-uploads-security-node-express/
       */
      tank.save((err, tank) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        
        res.status(200);
        res.send({ message: "Sensor was registered successfully!" });

      });

    } else {
      // No values to register user
      res.status(401).json(info);
    }
  }
};