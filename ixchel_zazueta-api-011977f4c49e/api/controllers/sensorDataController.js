const mongoose = require('mongoose');
const SensorData = mongoose.model('SensorData');
const WineryBarrelStorage = mongoose.model('WineryBarrelStorage');
const Sensor = mongoose.model('Sensor');
const Client = mongoose.model('Client');
const ObjectId = mongoose.Types.ObjectId;
const { Parser, transforms: { unwind } } = require('json2csv');

//function getLatest(req, res) {
module.exports.getLatest = (req, res, next) => {
  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_cava" || entry["name"] === "user_cava" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
	  SensorData.findOne({ sensor: req.params.id }).sort('-createdDate')
	  	.then(sensorInfo => sensorInfo ? res.json(sensorInfo) : res.sendStatus(404))
	    .catch(err => next(err));
  }
}


module.exports.getReport = (req, res, next) => {

  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_cava" || entry["name"] === "user_cava" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
    if(req.params.startDate && req.params.endDate ){
    
    SensorData.aggregate([
      {$project:
        {  
          client: 1,
          //minute :{$minute:"$createdDate"},
          createdDate:1,
          temp:1,
          humidity:1,
          sensor:1
        }
        
      }, 
          {$match: 
            {
              $and: [
              { client : ObjectId(req.params.id) },
              //{ minute: { "$in" : arrSteps }},
              { createdDate: {
              $gte: new Date(req.params.endDate)
              } 
            },
            { createdDate: {
              $lt: new Date(req.params.startDate)
              } 
            }
               ]
          }
        },
        {
          $sort: { createdDate : 1 }
        }
      ])
    .exec()
    .then(
      tankInfo => {

      if(tankInfo){
        console.log(tankInfo);
        res.json({ timeSeries : tankInfo});
      } 
      else {
        res.sendStatus(404);
      }

      },
      err => next(err)
      );
    }
  }
}

module.exports.getAllReport = (req, res, next) => {

  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_cava" || entry["name"] === "user_cava" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else { 
    if(req.params.id){
  
      SensorData.find({ client: ObjectId(req.params.id) }, 'createdDate temp humidity sensor')
      .populate("sensor")
    	.sort({ createdDate: 1 })
    	.then(
  	    tankInfo => {

  	    if(tankInfo){
  	      //res.json({ timeSeries : tankInfo });
  	      const fields = ['createdDate', 'temp', 'humidity', 'sensor.name'];
  	      const transforms = [unwind({ paths: ['sensor', 'sensor.name'] })];

  	          const json2csvParser = new Parser({ fields, transforms });
  	          const csv = json2csvParser.parse(tankInfo);
  	          //console.log(csv);
  	          res.setHeader('Content-disposition', 'attachment; filename=data.csv');
  	          res.set('Content-Type', 'text/csv');
  	          res.status(200).send(csv);
  	    } 
  	    else {
  	      res.sendStatus(404);
  	    }

  	    },
  	    err => next(err)
  	    
  	  ).catch(err => next(err));

  	}
  }
}

module.exports.getAllHumReport = (req, res, next) => {

  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_cava" || entry["name"] === "user_cava" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {

    if(req.params.id){
  
      SensorData.find({ client: ObjectId(req.params.id) }, 'createdDate humidity sensor')
      .populate("sensor")
    	.sort({ createdDate: 1 })
    	.then(
  	    tankInfo => {

  	    if(tankInfo){
  	      //res.json({ timeSeries : tankInfo });
  	      const fields = ['createdDate', 'humidity', 'sensor.name'];
  	      const transforms = [unwind({ paths: ['sensor', 'sensor.name'] })];

  	          const json2csvParser = new Parser({ fields, transforms });
  	          const csv = json2csvParser.parse(tankInfo);
  	          //console.log(csv);
  	          res.setHeader('Content-disposition', 'attachment; filename=data.csv');
  	          res.set('Content-Type', 'text/csv');
  	          res.status(200).send(csv);
  	    } 
  	    else {
  	      res.sendStatus(404);
  	    }

  	    },
  	    err => next(err)
  	    
  	  ).catch(err => next(err));

  	}
  }
   
}
