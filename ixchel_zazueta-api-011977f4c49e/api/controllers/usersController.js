const mongoose = require('mongoose');
const User = mongoose.model('User');
const Client = mongoose.model('Client');
const UserLog = mongoose.model('UserLog');
const MonitorUserLog = mongoose.model('MonitorUserLog');
const Role = mongoose.model('Role');
const { Parser, transforms: { unwind } } = require('json2csv');
const ObjectId = mongoose.Types.ObjectId;

//function getAll(req, res, next)
module.exports.getAll = (req, res, next) => {
    
    const currentUser = req.payload;
    // only allow admins 
    if (currentUser.roles.filter(function (entry) { return entry.name === "admin"; }).length == 0 ) {
        return res.status(401).json({ message: 'Unauthorized' });
    }

    /*should return all users of same client only if admin asks for them*/
    // if super_admin should return ALL users

    Client.findOne({  users : currentUser._id })
    .populate({
      path : 'users',
      populate : {
        path : 'roles'
      }
    })
    .exec(
      function (err, cliente) {

        if (err) {
            res.status(500).send({ message: err });
            return;
        }

        

        newArr = cliente.users.filter(function(item) {
              return String(item["_id"]) !== currentUser._id;
          });

        
        newArr = newArr.map(function(item){
            newitem = {
              id : item["_id"],
              name : item["name"],
              roles : item["roles"],
              email : item["email"]
            };
            return newitem;
          });

        //console.log(newArr);
        return res.json(newArr);
      }
    );

}

//function getCurrent(req, res, next) 
/*module.exports.getCurrent = (req, res, next) => {
    User.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}*/

//function getById(req, res, next) {
module.exports.getById = (req, res, next) => {
    const currentUser = req.payload;
    const id = req.params.id;
    // only allow admins to access other user records
    console.log(currentUser);
    if (id !== currentUser._id && currentUser.roles.filter(function (entry) { return entry.name === "admin"; }).length == 0 ) {
        return res.status(401).json({ message: 'Unauthorized' });
    }
    User.findById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

//function update(req, res, next) {
module.exports.update = (req, res, next) => {
    const currentUser = req.payload;
    const id = req.params.id;
    // only allow admins to access other user records
    if (id == currentUser._id || currentUser.roles.filter(function (entry) { return entry.name === "admin"; }).length == 0 ) {
        return res.status(401).json({ message: 'Unauthorized' });
    }

    
    // checar si hay que cambiar los roles ?
    if (req.body.roles) {

        newArrRoles = [];
        if(req.body.roles == "admin"){
          //make it admin in all that his admin is admin! :P except *admin*
          newArrRoles = currentUser.roles.filter(function(item) {
              return item["name"] !== "admin";
          });

          newArrRoles = newArrRoles.map(function(item){
            return item["name"];
          });

        } else {

          if(currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks"})){
            newArrRoles.push("user_tanks");
          }
          if(currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks_nocontrol"})){
            newArrRoles.push("user_tanks");
          }
          if(currentUser.roles.filter(function (entry) { return entry["name"] === "admin_cava"})){
            newArrRoles.push("user_cava");
          }
          if(currentUser.roles.filter(function (entry) { return entry["name"] === "admin_voltage"})){
            newArrRoles.push("user_voltage");
          }

        }

        console.log(newArrRoles);

        Role.find(
          {
            name: { $in: newArrRoles }
          },
          (err, rolesss) => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }

            console.log(rolesss);

            query = { "$set" : { 
                          name : req.body.name,
                          email : req.body.email,
                          roles : rolesss.map(role => role._id)
                        } 
            };

            User.updateOne({ _id : req.params.id}, query )
            .then((result) => {
                  if( req.body.newpassword != "" ){
                    // using callback
                    User.findOne({ _id: req.params.id }, (err, cpuser) => {
                      //console.log(cpuser);
                      cpuser.setPassword(req.body.newpassword);
                      cpuser.save((err, user) => {
                        if (err) {
                          res.status(500).send({ message: err });
                          return;
                        } else {
                          return res.status(200).json({
                            status: "Success",
                            message: "User was saved successfully",
                            data: result
                          });
                        }
                      });
                    });
                  } else {
                    // we dont need to change password

                    return res.status(200).json({
                      status: "Success",
                      message: "User was saved successfully",
                      data: result
                    });
                  }                  
            }).catch((error) => {
                  return res.status(500).json({
                      status: "Failed",
                      message: "Database Error",
                      data: error
                  });
            });
          }
        );
    } else {

      query = { "$set" : { 
                          name : req.body.name,
                          email : req.body.email
                        } 
            };

            User.updateOne({ _id : req.params.id}, query )
            .then((result) => {
                  if( req.body.newpassword != "" ){
                    // using callback
                    User.findOne({ _id: req.params.id }, (err, cpuser) => {
                      //console.log(cpuser);
                      cpuser.setPassword(req.body.newpassword);
                      cpuser.save((err, user) => {
                        if (err) {
                          res.status(500).send({ message: err });
                          return;
                        } else {
                          return res.status(200).json({
                            status: "Success",
                            message: "User was saved successfully",
                            data: result
                          });
                        }
                      });
                    });
                  } else {
                    // we dont need to change password

                    return res.status(200).json({
                      status: "Success",
                      message: "User was saved successfully",
                      data: result
                    });
                  }                  
            }).catch((error) => {
                  return res.status(500).json({
                      status: "Failed",
                      message: "Database Error",
                      data: error
                  });
            });

    }

}

//function _delete(req, res, next) {
module.exports._delete = (req, res, next) => {
    const currentUser = req.payload;
    const id = req.params.id;
    // only allow admins to access other user records
    console.log(currentUser);
    if (id !== currentUser._id && currentUser.roles.filter(function (entry) { return entry.name === "admin"; }).length == 0 ) {
        return res.status(401).json({ message: 'Unauthorized' });
    }

    User.findByIdAndRemove(req.params.id)
        .then( 
          function(){
            //should also update client user list !
            Client.updateOne({ users : id }, { "$pull": { users : ObjectId(id) } }, { safe: true })
              .then((result) => {
                  return res.status(200).json({
                      status: "Success",
                      message: "Resources Are Created Successfully",
                      data: result
                  });
              }).catch((error) => {
                  return res.status(500).json({
                      status: "Failed",
                      message: "Database Error",
                      data: error
                  });
              });

        })
        .catch(err => next(err));
}


module.exports.registerLog = (req, res) => {
  if (!req.payload._id) {
    res.status(401).json({
      message: 'UnauthorizedError: private profile'
    });
  } else {
    const userLog = new UserLog();

    if(req.body.userId && req.body.data && req.body.typeMsg && req.params.id){
      var name = "";
      if(req.body.typeMsg === "density"){
          name="density";
      } else {
          name="message";
      }
      userLog.name = name;
      userLog.user = req.body.userId;
      userLog.tank =req.params.id;
      userLog.info = req.body.data;
      userLog.createdDate = req.body.createdDate;
      /*
        Don’t forget that, in reality, this code would have a number of error traps, 
        validating form inputs and catching errors in the save function. 
        They’re omitted here to highlight the main functionality of the code, 
        but if you’d like a refresher, check out “Forms, File Uploads and Security with Node.js and Express”.
        https://www.sitepoint.com/forms-file-uploads-security-node-express/
       */
      userLog.save((err, client) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        
        res.status(200).send({ message: "UserLog was registered successfully!" });

      });

    } else {
      // No values to register user
      res.status(401).json(info);
    }

  }
};

module.exports.getAllUserLogs = (req, res) => {
  if (!req.payload._id) {
    res.status(401).json({
      message: 'UnauthorizedError: private profile'
    });
  } else {
    if(req.params.typeMsg && req.params.id){
      //console.log(req.params.typeMsg);
      names=req.params.typeMsg.split(',');
      //console.log(names);
       /*
        Don’t forget that, in reality, this code would have a number of error traps, 
        validating form inputs and catching errors in the save function. 
        They’re omitted here to highlight the main functionality of the code, 
        but if you’d like a refresher, check out “Forms, File Uploads and Security with Node.js and Express”.
        https://www.sitepoint.com/forms-file-uploads-security-node-express/
       */

      UserLog.find({ tank: req.params.id, name: { $in: names } }, "createdDate info name")
      .populate( "user", "name" )
      .sort({ createdDate: 1 })
      .then(
        tankInfo => {

        if(tankInfo){
          //res.json({ logs : tankInfo });
          //var data = json2csv({ data: tankInfo });
          //res.attachment('filename.csv');
          //res.status(200).send(data);
          const fields = ['createdDate', 'info', 'name', 'user.name'];
          const transforms = [unwind({ paths: ['user', 'user.name'] })];

          const json2csvParser = new Parser({ fields, transforms });
          const csv = json2csvParser.parse(tankInfo);
          //console.log(csv);
          res.setHeader('Content-disposition', 'attachment; filename=data.csv');
          res.set('Content-Type', 'text/csv');
          res.status(200).send(csv);
          //res.json({ logs : tankInfo });
        } 
        else {
          res.sendStatus(404);
        }

        },
        err => res.send(err)
        
      ).catch(err => res.send(err));


    } else {
      // No values 
      res.status(401).json(info);
    }
  }
  
};


module.exports.getUserLogs = (req, res) => {
  if (!req.payload._id) {
    res.status(401).json({
      message: 'UnauthorizedError: private profile'
    });
  } else {

    if(req.params.typeMsg && req.params.id){
      
      names=req.params.typeMsg.split(',');
       /*
        Don’t forget that, in reality, this code would have a number of error traps, 
        validating form inputs and catching errors in the save function. 
        They’re omitted here to highlight the main functionality of the code, 
        but if you’d like a refresher, check out “Forms, File Uploads and Security with Node.js and Express”.
        https://www.sitepoint.com/forms-file-uploads-security-node-express/
       */

      UserLog.find({ tank: req.params.id, name: { $in: names }, createdDate: {
        $gte: new Date(req.params.startDate),
        $lt: new Date(req.params.endDate)
        }
      }, "createdDate info name")
      .populate( "user", "name"  )
      .sort({ createdDate: 1 })
      .then(
        tankInfo => {

          if(tankInfo){


              //res.json({ logs : tankInfo });
              // falta agregar los datos del tanque!
              const fields = ['createdDate', 'info', 'name', 'user.name'];
              const transforms = [unwind({ paths: ['user', 'user.name'] })];

              const json2csvParser = new Parser({ fields, transforms });
              const csv = json2csvParser.parse(tankInfo);
              //console.log(csv);
              res.setHeader('Content-disposition', 'attachment; filename=data.csv');
              res.set('Content-Type', 'text/csv');
              res.status(200).send(csv);

          } 
          else {
            res.sendStatus(404);
          }

        },
        err => next(err)
        
      ).catch(err => next(err));


    } else {
      // No values 
      res.status(401);
    }
  }  
};



module.exports.registerMonitorLog = (req, res) => {
  if (!req.payload._id) {
    res.status(401).json({
      message: 'UnauthorizedError: private profile'
    });
  } else {
    const userLog = new MonitorUserLog();

    if(req.body.userId && req.body.data && req.body.typeMsg && req.params.id){
      var name = "";
      if(req.body.typeMsg === "density"){
          name="density";
      } else {
          name="message";
      }
      userLog.name = name;
      userLog.user = req.body.userId;
      userLog.monitortank =req.params.id;
      userLog.info = req.body.data;
      userLog.createdDate = req.body.createdDate;
      /*
        Don’t forget that, in reality, this code would have a number of error traps, 
        validating form inputs and catching errors in the save function. 
        They’re omitted here to highlight the main functionality of the code, 
        but if you’d like a refresher, check out “Forms, File Uploads and Security with Node.js and Express”.
        https://www.sitepoint.com/forms-file-uploads-security-node-express/
       */
      userLog.save((err, client) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        
        res.status(200).send({ message: "MonitorUserLog was registered successfully!" });

      });

    } else {
      // No values to register user
      res.status(401).json(info);
    }
  }
};

module.exports.getAllMonitorUserLogs = (req, res) => {
  if (!req.payload._id) {
    res.status(401).json({
      message: 'UnauthorizedError: private profile'
    });
  } else {
    if(req.params.typeMsg && req.params.id){
      //console.log(req.params.typeMsg);
      names=req.params.typeMsg.split(',');
      //console.log(names);
       /*
        Don’t forget that, in reality, this code would have a number of error traps, 
        validating form inputs and catching errors in the save function. 
        They’re omitted here to highlight the main functionality of the code, 
        but if you’d like a refresher, check out “Forms, File Uploads and Security with Node.js and Express”.
        https://www.sitepoint.com/forms-file-uploads-security-node-express/
       */

      MonitorUserLog.find({ monitortank: req.params.id, name: { $in: names } }, "createdDate info name")
      .populate( "user", "name" )
      .sort({ createdDate: 1 })
      .then(
        tankInfo => {

        if(tankInfo){
          //res.json({ logs : tankInfo });
          //var data = json2csv({ data: tankInfo });
          //res.attachment('filename.csv');
          //res.status(200).send(data);
          const fields = ['createdDate', 'info', 'name', 'user.name'];
          const transforms = [unwind({ paths: ['user', 'user.name'] })];

          const json2csvParser = new Parser({ fields, transforms });
          const csv = json2csvParser.parse(tankInfo);
          //console.log(csv);
          res.setHeader('Content-disposition', 'attachment; filename=data.csv');
          res.set('Content-Type', 'text/csv');
          res.status(200).send(csv);
          //res.json({ logs : tankInfo });
        } 
        else {
          res.sendStatus(404);
        }

        },
        err => res.send(err)
        
      ).catch(err => res.send(err));


    } else {
      // No values 
      res.status(401).json(info);
    }
  }
  
};


module.exports.getMonitorUserLogs = (req, res) => {
  if (!req.payload._id) {
    res.status(401).json({
      message: 'UnauthorizedError: private profile'
    });
  } else {
    if(req.params.typeMsg && req.params.id){
      
      names=req.params.typeMsg.split(',');
       /*
        Don’t forget that, in reality, this code would have a number of error traps, 
        validating form inputs and catching errors in the save function. 
        They’re omitted here to highlight the main functionality of the code, 
        but if you’d like a refresher, check out “Forms, File Uploads and Security with Node.js and Express”.
        https://www.sitepoint.com/forms-file-uploads-security-node-express/
       */

      MonitorUserLog.find({ monitortank: req.params.id, name: { $in: names }, createdDate: {
        $gte: new Date(req.params.startDate),
        $lt: new Date(req.params.endDate)
        }
      }, "createdDate info name")
      .populate( "user", "name"  )
      .sort({ createdDate: 1 })
      .then(
        tankInfo => {

          if(tankInfo){


              //res.json({ logs : tankInfo });
              // falta agregar los datos del tanque!
              const fields = ['createdDate', 'info', 'name', 'user.name'];
              const transforms = [unwind({ paths: ['user', 'user.name'] })];

              const json2csvParser = new Parser({ fields, transforms });
              const csv = json2csvParser.parse(tankInfo);
              //console.log(csv);
              res.setHeader('Content-disposition', 'attachment; filename=data.csv');
              res.set('Content-Type', 'text/csv');
              res.status(200).send(csv);

          } 
          else {
            res.sendStatus(404);
          }

        },
        err => next(err)
        
      ).catch(err => next(err));


    } else {
      // No values 
      res.status(401);
    }
  }
};
