const mongoose = require('mongoose');
const Voltage = mongoose.model('Voltage');
const Client = mongoose.model('Client');
const ObjectId = mongoose.Types.ObjectId;
const { Parser, transforms: { unwind } } = require('json2csv');

module.exports.getReport = (req, res, next) => {
  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_voltage" || entry["name"] === "user_voltage" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
	  if(req.params.startDate && req.params.endDate ){
		
		lngtSteps = 60/req.params.stepMins;
		arrSteps = [];
		for (var i = 0; i < lngtSteps; i++) {
			stp = req.params.stepMins*i;
			arrSteps.push(stp);
		}
		

		Voltage.aggregate([
			/*{$project:
				{	
					client: 1,
					minute :{$minute:"$createdDate"},
					createdDate:1,
					volt1:1,
					volt2:1,
					volt3:1,
					timestamp: 1
				}
				
			}, 
	        {$match: 
	        	{
	        		$and: [
	        		{ client : ObjectId(req.params.id) },
	        		{ minute: { "$in" : arrSteps }},
	        		{ createdDate: {
					    $gte: new Date(req.params.endDate)
				      } 
				  	},
				  	{ createdDate: {
					    $lt: new Date(req.params.startDate)
				      } 
				  	}
	       			]
	    		}
	    	}*/
	    	{$project:
				{	
					client: 1,
					minute :{$minute:"$createdDate"},
					second :{$second:"$createdDate"},
					createdDate:1,
					volt1:1,
					volt2:1,
					volt3:1,
					timestamp: 1
				}
				
			}, 
	        {$match: 
	        	{
	        		$and: [
	        		{ client : ObjectId(req.params.id) },
	        		//{ minute: { "$in" : arrSteps }},
	        		//{ second: { $gt: 0 }},
	        		//{ second: { $lt: 6 }},
	        		{ createdDate: {
					    $gte: new Date(req.params.endDate)
				      } 
				  	},
				  	{ createdDate: {
					    $lt: new Date(req.params.startDate)
				      } 
				  	}
	       			]
	    		}
	    	},
	    	{
			    $group: {
			        _id: { 
			        	  createdDate : {
			        	  "$toDate": {
			        	  //"$add": [
			        		 //{
					              "$subtract": [
					                  { "$toLong": "$createdDate"  },
					                  { "$mod": [ { "$toLong":  "$createdDate" }, 1000 * 60 * req.params.stepMins ] }
					              ]
					          }}//,
					          //{ $multiply : [ {$hour:"$createdDate"} , 3600*1000 ]}
					     //]
					 	//}
					    //interval: { $trunc: { $divide: [ {$minute:"$createdDate"}, Number(req.params.stepMins) ]}}
			        },
			        volt1:  { $avg: "$volt1" },
					volt2: { $avg: "$volt2" },
					volt3: { $avg: "$volt3" },
					timestamp : { $avg: "$timestamp" }
			    }
			},
			{
				$sort: { _id : 1 }
			}
	    ])
		.exec()
		.then(
			tankInfo => {

			if(tankInfo){
				//console.log(tankInfo);
				res.json({ timeSeries : tankInfo});
			} 
			else {
				res.sendStatus(404);
			}

		  },
		  err => next(err)
	    );
	  }
  }
}

module.exports.getAllReport = (req, res, next) => {

  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_voltage" || entry["name"] === "user_voltage" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
	  if(req.params.id){
		
	  	Voltage.find({ client: req.params.id })
		.sort({ timestamp: 1 })
		.then(
		  tankInfo => {

			if(tankInfo){
				//res.json({ timeSeries : tankInfo });
				const fields = ['timestamp', 'volt1', 'volt2', 'volt3'];
		        //const transforms = [unwind({ paths: ['user', 'user.name'] })];

		        const json2csvParser = new Parser({ fields });
		        const csv = json2csvParser.parse(tankInfo);
		        //console.log(csv);
		        res.setHeader('Content-disposition', 'attachment; filename=data.csv');
		        res.set('Content-Type', 'text/csv');
		        res.status(200).send(csv);
			} 
			else {
				res.sendStatus(404);
			}

		  },
		  err => next(err)
			
		).catch(err => next(err));

	  }
  }
}