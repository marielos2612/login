const mongoose = require('mongoose');
const passport = require('passport');
const User = mongoose.model('User');
const Client = mongoose.model('Client');
const Role = mongoose.model('Role');
const ObjectId = mongoose.Types.ObjectId;


module.exports.register = (req, res) => {
  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  }

  Client.findOne({ name : req.body.clientName }, { users : 1 })
  .then((result) => {
    if(result.users.length >= 10){
      // cannot create more users for that client! 
      return res.status(500).send("Cannot create more users for that client.");

    } else {

      const user = new User();
      if(req.body.name && req.body.email && req.body.password){

        user.name = req.body.name;
        user.email = req.body.email;
        user.setPassword(req.body.password);
        /*
          Don’t forget that, in reality, this code would have a number of error traps, 
          validating form inputs and catching errors in the save function. 
          They’re omitted here to highlight the main functionality of the code, 
          but if you’d like a refresher, check out “Forms, File Uploads and Security with Node.js and Express”.
          https://www.sitepoint.com/forms-file-uploads-security-node-express/
         */
        user.save((err, user) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          if (req.body.roles) {

            newArrRoles = [];

            if(req.body.roles == "admin"){
              //make it admin in all that his admin is admin! :P except *admin*
              newArrRoles = currentUser.roles.filter(function(item) {
                  return item["name"] !== "admin";
              });

              newArrRoles = newArrRoles.map(function(item){
                return item["name"];
              });

            } else {

              if(currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks"})){
                newArrRoles.push("user_tanks");
              }
              if(currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks_nocontrol"})){
                newArrRoles.push("user_tanks");
              }
              if(currentUser.roles.filter(function (entry) { return entry["name"] === "admin_cava"})){
                newArrRoles.push("user_cava");
              }
              if(currentUser.roles.filter(function (entry) { return entry["name"] === "admin_voltage"})){
                newArrRoles.push("user_voltage");
              }

            }

            console.log(newArrRoles);

            Role.find(
              {
                name: { $in: newArrRoles }
              },
              (err, roles) => {
                if (err) {
                  res.status(500).send({ message: err });
                  return;
                }

                console.log(roles);

                user.roles = roles.map(role => role._id);
                user.save((err,savedUs) => {
                  if (err) {
                    res.status(500).send({ message: err });
                    return;
                  }

                  // it should have all the roles the creator has but in user - unless admin
                  //so check all of creators roles!
                  Client.updateOne({ name : req.body.clientName }, { "$push": { users : ObjectId(savedUs._id) } }, 
                    {new: true, safe: true, upsert: true }).then((result) => {
                      return res.status(201).json({
                          status: "Success",
                          message: "Resources Are Created Successfully",
                          data: result
                      });
                  }).catch((error) => {
                      return res.status(500).json({
                          status: "Failed",
                          message: "Database Error",
                          data: error
                      });
                  });

                  //res.status(200);
                  //res.send({ message: "User was registered successfully!" });
                
                });
              }
            );
          } else {
            Role.findOne({ name: "user" }, (err, role) => {
              if (err) {
                res.status(500).send({ message: err });
                return;
              }

              user.roles = [role._id];
              user.save( (err,savedUs) => {
                if (err) {
                  res.status(500).send({ message: err });
                  return;
                }
                Client.updateOne({ name : req.body.clientName }, { "$push": { users : ObjectId(savedUs._id) } }, 
                  {new: true, safe: true, upsert: true }).then((result) => {
                    return res.status(201).json({
                        status: "Success",
                        message: "Resources Are Created Successfully",
                        data: result
                    });
                }).catch((error) => {
                    return res.status(500).json({
                        status: "Failed",
                        message: "Database Error",
                        data: error
                    });
                });

              });
            });
          }

        });

      } else {
        // No values to register user
        res.status(401).json(info);
      }
    }
  }).catch((error) => {
    return res.status(500)
            .send({
              status: "Failed",
              message: "Database Error",
              data: error
    });
  
  });

};

module.exports.registerWClient = (req, res) => {
  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  }

  console.log(req.body);

  const user = new User();
  const client = new Client();

  if(req.body.name && req.body.email && req.body.password && req.body.roles ){

    user.name = req.body.name;
    user.email = req.body.email;
    user.setPassword(req.body.password);

    user.save((err, user) => {
          if (err) {
            return res.status(500).send( err );
          }

          Role.find(
              {
                name: { $in: req.body.roles }
              },
              (err, roles) => {
                if (err) {
                  res.status(500).send({ message: err });
                  return;
                }

                console.log(roles);

                user.roles = roles.map(role => role._id);
                user.save((err,savedUs) => {
                  if (err) {
                    return res.status(500).send(err);
                  }

                  client.name = req.body.clientName;
                  client.users = [ ObjectId(savedUs._id) ];
                  client.save((errorrr, clientesito) => { 
                    if (errorrr) {
                      return res.status(500).send(errorrr);
                    }

                    return res.status(200).send({ message: "User And Client was registered successfully!" });

                  });
                
                });
              }
          );
       

        });

  } else {
    // No values to register user
    res.status(404).json(info);
  }

};



module.exports.registerWOPermWOClient = (req, res) => {
  
  const user = new User();
  //const client = new Client();

  if(req.body.name && req.body.email && req.body.password ){

    user.name = req.body.name;
    user.email = req.body.email;
    user.setPassword(req.body.password);

    user.save((err, user) => {
          if (err) {
            return res.status(500).send( err );
          }

          Role.find(
              {
                name: { $in: ["admin"] }
              },
              (err, roles) => {
                if (err) {
                  res.status(500).send({ message: err });
                  return;
                }

                console.log(roles);

                user.roles = roles.map(role => role._id);
                user.save((err,savedUs) => {
                  if (err) {
                    return res.status(500).send(err);
                  }

                  return res.status(200).send({ message: "User was registered successfully!" });
                
                });
              }
          );
       

        });

  } else {
    // No values to register user
    res.status(404).json(info);
  }

};

module.exports.login = (req, res) => {
  passport.authenticate('local', (err, user, info) => {
    // If Passport throws/catches an error
    if (err) {
      res.status(404).json(err);
      return;
    }

    // If a user is found
    if (user) {
      const token = user.generateJwt();
      res.status(200);
      res.json({
        token: token
      });
    } else {
      // If user is not found
      res.status(401).json(info);
    }
  })(req, res);
};


module.exports.changePassword = (req, res) => {
  passport.authenticate('local', (err, user, info) => {
    // If Passport throws/catches an error
    if (err) {
      res.status(404).json(err);
      return;
    }

    // If a user is found
    if (user) {

      console.log(req.body);
      console.log(user);

      user.setPassword(req.body.newpassword);
      user.save((err, user) => {
                        if (err) {
                          res.status(500).send({ message: err });
                          return;
                        } else {
                          return res.status(200).json({
                            status: "Success",
                            message: "User was saved successfully",
                            data: result
                          });
                        }
                      });

     /* const token = user.generateJwt();
      res.status(200);
      res.json({
        token: token
      });*/

    } else {
      // If user is not found
      res.status(401).json(info);
    }
  })(req, res);
};