const mongoose = require('mongoose');
const Client = mongoose.model('Client');
const Tank = mongoose.model('Tank');
const TanksSystem = mongoose.model('TanksSystem');

module.exports.register = (req, res) => {

  const currentUser = req.payload;
  // only allow tank users to access records
  // it also checks if it has acces to tank!!
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks" || entry["name"] === "super_admin"  });
  if ( isItAdmin.length == 0 ) {
    return res.status(401).send({ message: 'Unauthorized' });
  } else {

    const tanksSystem = new TanksSystem();

    if(req.body.client && req.body.tanks && req.body.name){

      tanksSystem.name = req.body.name;
      tanksSystem.client = req.body.client;
      tanksSystem.tanks = req.body.tanks;
      /*
        Don’t forget that, in reality, this code would have a number of error traps, 
        validating form inputs and catching errors in the save function. 
        They’re omitted here to highlight the main functionality of the code, 
        but if you’d like a refresher, check out “Forms, File Uploads and Security with Node.js and Express”.
        https://www.sitepoint.com/forms-file-uploads-security-node-express/
       */
      tanksSystem.save((err, tanksSystem) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        res.status(200);
        res.send({ message: "Tanks System was registered successfully!" });

      });

    } else {
      // No values to register user
      res.status(401).json(info);
    }
  }
};


//function update(req, res, next) {
module.exports.update = (req, res, next) => {
  const currentUser = req.payload;
  // only allow tank users to access records
  // it also checks if it has acces to tank!!
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks" || entry["name"] === "super_admin" || entry["name"] === "user_tanks"  });
  if ( isItAdmin.length == 0 ) {
    return res.status(401).send({ message: 'Unauthorized' });
  } else {
    TanksSystem.updateOne({ _id : req.params.id}, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
  }
}


//function getAll(req, res, next)
module.exports.getAll = (req, res, next) => {
  const currentUser = req.payload;
  // only allow tank users to access records
  // it also checks if it has acces to tank!!
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks" || entry["name"] === "super_admin" || entry["name"] === "user_tanks"  });
  if ( isItAdmin.length == 0 ) {
    return res.status(401).send({ message: 'Unauthorized' });
  } else {
    TanksSystem.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
  }
}

//function getCurrent(req, res, next) 
/*module.exports.getCurrent = (req, res, next) => {
    User.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}*/

//function getById(req, res, next) {
module.exports.getById = (req, res, next) => {
  const currentUser = req.payload;
  // only allow tank users to access records
  // it also checks if it has acces to tank!!
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks" || entry["name"] === "super_admin" || entry["name"] === "user_tanks"  });
  if ( isItAdmin.length == 0 ) {
    return res.status(401).send({ message: 'Unauthorized' });
  } else {
    TanksSystem.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
  }
}

//function _delete(req, res, next) {
module.exports._delete = (req, res, next) => {
  const currentUser = req.payload;
  // only allow tank users to access records
  // it also checks if it has acces to tank!!
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks" || entry["name"] === "super_admin"  });
  if ( isItAdmin.length == 0 ) {
    return res.status(401).send({ message: 'Unauthorized' });
  } else {
    TanksSystem.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
  }
}
