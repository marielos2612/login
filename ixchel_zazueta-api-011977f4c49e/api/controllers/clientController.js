const mongoose = require('mongoose');
const Client = mongoose.model('Client');

//function getAll(req, res, next)
module.exports.getAll = (req, res, next) => {

  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
    Client.find({})
        .then(users => res.json(users))
        .catch(err => next(err));
  }
}

//function getById(req, res, next) {
module.exports.getById = (req, res, next) => {
    Client.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

//function update(req, res, next) {
module.exports.update = (req, res, next) => {
  if(!req.file) {
        return res.status(500).send({ message: 'Upload fail'});
    } else {
        req.body.imageUrl = req.file.path;
        //req.body.imageUrl = "uploads/"+req.file.filename;
        Client.updateOne({ _id : req.params.id}, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
    }
  
}

//function _delete(req, res, next) {
module.exports._delete = (req, res, next) => {
    Client.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

module.exports.register = (req, res, next) => {
   if(!req.file) {
        return res.status(500).send({ message: 'Upload fail'});
    } else {
        req.body.imageUrl = 'http://localhost:3000/images/' + req.file.filename;
        Client.create(req.body, function (err, gallery) {
            if (err) {
                console.log(err);
                return next(err);
            }
            res.json(gallery);
        });
    }
    /*
      Don’t forget that, in reality, this code would have a number of error traps, 
      validating form inputs and catching errors in the save function. 
      They’re omitted here to highlight the main functionality of the code, 
      but if you’d like a refresher, check out “Forms, File Uploads and Security with Node.js and Express”.
      https://www.sitepoint.com/forms-file-uploads-security-node-express/
     */
  //const client = new Client();
  /*
  if(req.body.users && req.body.name ){

    client.name = req.body.name;
    client.users = req.body.users;
    client.save((err, client) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
      
      res.status(200);
      res.send({ message: "Client was registered successfully!" });

    });

  } else {
    // No values to register user
    res.status(401).json(info);
  }
  */
};