const mongoose = require('mongoose');
const TankInfoPerFiveSeconds = mongoose.model('TankInfoPerFiveSeconds');
const TheLatestTankInfo = mongoose.model('LatestTankInfo');
const TanksSystem = mongoose.model('TanksSystem');
const Voltage = mongoose.model('Voltage');
const Tank = mongoose.model('Tank');
const Client = mongoose.model('Client');
const UserLog = mongoose.model('UserLog');
const Environment = mongoose.model('Environment');
const cryptoJS = require("crypto-js");
const ObjectId = mongoose.Types.ObjectId;
const { Parser, transforms: { unwind } } = require('json2csv');

const mqtt = require('mqtt');

var options={
  username:"redone",
  password:"1Red.xxcvb",
  clean:true
};

const mqttClient  = mqtt.connect('mqtt://13.52.72.95',options);

//function getLatest(req, res) {
module.exports.getLatest = (req, res, next) => {
 
  const currentUser = req.payload;
  // only allow tank users to access records
  // it also checks if it has acces to tank!!
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks" || entry["name"] === "user_tanks" || entry["name"] === "super_admin" });
  if ( isItAdmin.length == 0 ) {
    return res.status(401).send({ message: 'Unauthorized' });
  } else {

  	Client.findOne({ users : currentUser._id }, function(error, cliente) {

  		if(cliente) {
	  		TanksSystem.findOne({ client : cliente._id , tanks: req.params.id  }, function(error1, tanksyst) {

	  			if(tanksyst){
	  				TheLatestTankInfo.findOne({ tank: req.params.id }).sort('-createdDate')
				  	.then(tankInfo => tankInfo ? res.json(tankInfo) : res.sendStatus(404))
				    .catch(err => next(err));

	  			} else {
	  				return res.status(401).send({ message: 'Unauthorized' });
	  			}
	  			

	  		});
  		} else {
  			return res.status(401).send({ message: 'Unauthorized' });
  		}

  	});

  	  	
  }
}

module.exports.getReport = (req, res, next) => {
  if (!req.payload._id) {
    res.status(401).json({
      message: 'UnauthorizedError: private profile'
    });
  } else {
    // Otherwise continue
    //userId = req.payload._id;

	  const currentUser = req.payload;
	  // only allow tank users to access records
	  // it also checks if it has acces to tank!!
	  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks" || entry["name"] === "user_tanks" || entry["name"] === "super_admin" });
	  if ( isItAdmin.length == 0 ) {
	    return res.status(401).send({ message: 'Unauthorized' });
	  } else {

		  if(req.params.startDate && req.params.endDate ){
			
			lngtSteps = 60/req.params.stepMins;
			arrSteps = [];
			for (var i = 0; i < lngtSteps; i++) {
				stp = req.params.stepMins*i;
				arrSteps.push(stp);
			}
			
			//console.log(new Date(req.params.endDate));

			TankInfoPerFiveSeconds.aggregate([
				/*{$project:
					{	
						tank: 1,
						minute :{$minute:"$createdDate"},
						//second :{$second:"$createdDate"},
						createdDate:1,
						temp:1,
						maxTemp:1,
						minTemp:1,
						controlEnabled:1
					}
					
				}, */
		        { $match: 
		        	{
		        		$and: [
		        		{ tank : ObjectId(req.params.id) },
		        		//{ minute: { "$in" : arrSteps }},
		        		//{ second: { $gt: 0 }},
		        		//{ second: { $lt: 6 }},
		        		{ createdDate: {
						    $gte: new Date(req.params.endDate)
					      } 
					  	},
					  	{ createdDate: {
						    $lt: new Date(req.params.startDate)
					      } 
					  	}
		       			]
		    		}
		    	},
		    	{
				    $group: {
				        _id: { 
				        	  createdDate : {
				        	  "$toDate": {
				        	  //"$add": [
				        		 //{
						              "$subtract": [
						                  { "$toLong": "$createdDate"  },
						                  { "$mod": [ { "$toLong":  "$createdDate" }, 1000 * 60 * req.params.stepMins ] }
						              ]
						          }}//,
						          //{ $multiply : [ {$hour:"$createdDate"} , 3600*1000 ]}
						     //]
						 	//}
						    //interval: { $trunc: { $divide: [ {$minute:"$createdDate"}, Number(req.params.stepMins) ]}}
				        },
				        temp: { $avg: "$temp" },
				        //createdDate : _id,
				        maxTemp: { $avg: "$maxTemp" },
						minTemp: { $avg: "$minTemp" },
						controlEnabled: { $first: "$controlEnabled" },
				    }
				},
				{
					$sort: { _id : 1 }
				}
		    	
		    ])
			.exec()
			.then(
				tankInfo => {

				if(tankInfo){
					if(req.params.userLogs == "true"){
						UserLog.find({tank:req.params.id}).exec(function(err, userlogs) {
							res.json({ timeSeries : tankInfo , userLogs : userlogs});
						});
					} else {
						res.json({ timeSeries : tankInfo});
					}
				} 
				else {
					res.sendStatus(404);
				}

			  },
			  err => next(err)
		    );
		  }
		}
	}
}

module.exports.getEnvironmentReport = (req, res, next) => {
  if (!req.payload._id) {
    res.status(401).json({
      message: 'UnauthorizedError: private profile'
    });
  } else {
    // Otherwise continue
    //userId = req.payload._id;

  if(req.params.startDate && req.params.endDate ){
	
	lngtSteps = 60/req.params.stepMins;
	arrSteps = [];
	for (var i = 0; i < lngtSteps; i++) {
		stp = req.params.stepMins*i;
		arrSteps.push(stp);
	}
	console.log(arrSteps);

	Environment.aggregate([
		{$project:
			{	
				client: 1,
				minute :{$minute:"$createdDate"},
				second :{$second:"$createdDate"},
				createdDate:1,
				temp:1
			}
			
		}, 
        {$match: 
        	{
        		$and: [
        		{ client : ObjectId(req.params.id) },
        		//{ minute: { "$in" : arrSteps }},
        		//{ second: { $gt: 0 }},
        		//{ second: { $lt: 6 }},
        		{ createdDate: {
				    $gte: new Date(req.params.endDate)
			      } 
			  	},
			  	{ createdDate: {
				    $lt: new Date(req.params.startDate)
			      } 
			  	}
       			]
    		}
    	},
    	{
		    $group: {
		        _id: { 
		        	  createdDate : {
		        	  "$toDate": {
		        	  //"$add": [
		        		 //{
				              "$subtract": [
				                  { "$toLong": "$createdDate"  },
				                  { "$mod": [ { "$toLong":  "$createdDate" }, 1000 * 60 * req.params.stepMins ] }
				              ]
				          }}//,
				          //{ $multiply : [ {$hour:"$createdDate"} , 3600*1000 ]}
				     //]
				 	//}
				    //interval: { $trunc: { $divide: [ {$minute:"$createdDate"}, Number(req.params.stepMins) ]}}
		        },
		        temp: { $avg: "$temp" },
		        //createdDate : _id,
		        //maxTemp: { $avg: "$maxTemp" },
				//minTemp: { $avg: "$minTemp" },
				//controlEnabled: { $first: "$controlEnabled" },
		    }
		},
		{
			$sort: { _id : 1 }
		}
    ])
	.exec()
	.then(
		tankInfo => {

		if(tankInfo){
			res.json({ timeSeries : tankInfo});
		} else {
			res.sendStatus(404);
		}

	  },
	  err => next(err)
    );
  }

  }
}

module.exports.getAllReport = (req, res, next) => {
  if (!req.payload._id) {
    res.status(401).json({
      message: 'UnauthorizedError: private profile'
    });
  } else {
    // Otherwise continue
    //userId = req.payload._id;

	  const currentUser = req.payload;
	  // only allow tank users to access records
	  // it also checks if it has acces to tank!!
	  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks" || entry["name"] === "user_tanks" || entry["name"] === "super_admin" });
	  if ( isItAdmin.length == 0 ) {
	    return res.status(401).send({ message: 'Unauthorized' });
	  } else {

		  if(req.params.id){
			
		  	TankInfoPerFiveSeconds.find({ tank: req.params.id }, 'tank createdDate temp maxTemp minTemp')
			.sort({ createdDate: 1 })
			.then(
			  tankInfo => {

				if(tankInfo){
					//res.json({ timeSeries : tankInfo });
					const fields = ['createdDate', 'temp', 'maxTemp', 'minTemp'];
			        //const transforms = [unwind({ paths: ['user', 'user.name'] })];

			        const json2csvParser = new Parser({ fields });
			        const csv = json2csvParser.parse(tankInfo);
			        //console.log(csv);
			        res.setHeader('Content-disposition', 'attachment; filename=data.csv');
			        res.set('Content-Type', 'text/csv');
			        res.status(200).send(csv);
				} 
				else {
					res.sendStatus(404);
				}

			  },
			  err => next(err)
				
			).catch(err => next(err));
	  	}
	  }
  }
}

mqttClient.on('connect', function () {
  console.log("connected to mqtt");    
});

const key = "2KX197208564zuBC";

function encrypt(data) {

	var wordArray = cryptoJS.enc.Utf8.parse(data);
	var base64 = cryptoJS.enc.Base64.stringify(wordArray);
	var ciphertext = cryptoJS.AES.encrypt(base64, key).toString();

	console.log(base64);
	console.log(ciphertext);

    return ciphertext;
}

module.exports.changeControlStatus = (req, res) => {
  if (!req.payload._id) {
    res.status(401).json({
      message: 'UnauthorizedError: private profile'
    });
  } else {
    // Otherwise continue
    //userId = req.payload._id;

	  const currentUser = req.payload;
	  // only allow tank users to access records
	  // it also checks if it has acces to tank!!
	  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks" || entry["name"] === "user_tanks" || entry["name"] === "super_admin" });
	  if ( isItAdmin.length == 0 ) {
	    return res.status(401).send({ message: 'Unauthorized' });
	  } else {

	    userLog = new UserLog();
	    console.log(req.params.id);
	    //console.log(req);
	    console.log(req.body);
	    if (mqttClient.connected == true){
		  	//it should also save in the db what user changed the status
		    Tank.findById(req.params.id, function(error, tank) {

			    if (!error) {
			    	//ixenbc/tanques/control/1/Control/
			    	Client.findById(tank.client, function(error, client_) {
			    		var topic = "ixenbc/"+client_.name+"/tanques/control/"+tank.name+"/Control/";
				    	var msg = encrypt(req.body.data);

				    	console.log("publish to >> "+topic+" << the message : "+req.body.data+" ... "+msg);
				    	mqttClient.publish(topic,msg.toString());

					    userLog.user = req.body.userId;
					    userLog.tank = tank._id;
					    userLog.name = "Control Status";
					    userLog.info = "Cambio del estado de control a "+req.body.data;//((msg)? "verdadero":"falso");
					    userLog.createdDate = Date.now();
					    userLog.save((err, userlog) => {
					        if (err) {
					          console.log("couldnt save userLog");
					          res.status(404).send({ message: err });
					        }
					        //return true;
					        res.json(userlog);
					    });
			    	});
			    	
			    } else {
			      console.log("couldnt find Tank to save time series");
			      res.status(404).send({ message: error });
			      //return false;
			    }

		  	});

	    }
	}
  }
};

module.exports.changeControlTemperature = (req, res) => {
  if (!req.payload._id) {
    res.status(401).json({
      message: 'UnauthorizedError: private profile'
    });
  } else {
    // Otherwise continue
    //userId = req.payload._id;
	  const currentUser = req.payload;
	  // only allow tank users to access records
	  // it also checks if it has acces to tank!!
	  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks" || entry["name"] === "user_tanks" || entry["name"] === "super_admin" });
	  if ( isItAdmin.length == 0 ) {
	    return res.status(401).send({ message: 'Unauthorized' });
	  } else {    
	    //console.log(req.params.id);
	    //console.log(req);
	    //console.log(req.body);
	    if (mqttClient.connected == true){
		  	//it should also save in the db what user changed the status
		    Tank.findById(req.params.id, function(error, tank) {

			    if (!error) {
			    	//ixenbc/tanques/control/1/Control/
			    	Client.findById(tank.client, function(error, client_) {
			    		var topic = "ixenbc/"+client_.name+"/tanques/control/"+tank.name;//+"/Control/";
				    	//var msg = req.body.data;
				    	//console.log("publish to >> "+topic+" << the message : "+req.body);
				    	//expected: userId, Tmin, Tmax,
				    	if(req.body.Tmin != null){
				    		newTopic = topic+"/Tmin/";
				    		var msg1 = encrypt(req.body.Tmin.toString());
				    		console.log("publish to >> "+newTopic+" << the message : "+msg1);
				    		mqttClient.publish(newTopic,msg1);
				    	}
				    	if(req.body.Tmax != null){
				    		newTopic = topic+"/Tmax/";
				    		var msg2 = encrypt(req.body.Tmax.toString());
				    		console.log("publish to >> "+newTopic+" << the message : "+msg2);
				    		mqttClient.publish(newTopic,msg2);
				    	}
				    	if(req.body.Tmax != null || req.body.Tmin != null){
					    	userLog = new UserLog();
						    userLog.user = req.body.userId;
						    userLog.tank = tank._id;
						    userLog.name = "Control Temp";
						    userLog.info = "Cambio de rangos de control de temperatura, Tmin: "+req.body.Tmin+", Tmax:"+req.body.Tmax+"";
						    userLog.createdDate = Date.now();
						    userLog.save((err, userlog) => {
						        if (err) {
						          console.log("couldnt save userLog");
						          res.status(404).send({ message: err });
						        }
						        //return true;
						        res.json(userlog);
						    });
						} else {
							res.status(304).json({ message: "Nothing to change." });
						}
			    	});
			    	
			    } else {
			      console.log("couldnt find Tank to save time series");
			      res.status(404).send({ message: error });
			      //return false;
			    }

		  	});

	    }
	}
  }
};
