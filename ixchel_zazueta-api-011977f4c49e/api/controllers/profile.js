const mongoose = require('mongoose');
const User = mongoose.model('User');
const Role = mongoose.model('Role');
const TanksSystem = mongoose.model('TanksSystem');
const MonitorTanksSystem = mongoose.model('MonitorTanksSystem');
const Tank = mongoose.model('Tank');
const Client = mongoose.model('Client');
const TankInfoPerFiveSeconds = mongoose.model('TankInfoPerFiveSeconds');
const WineryBarrelStorage = mongoose.model('WineryBarrelStorage');
const Sensor = mongoose.model('Sensor');
const SensorData = mongoose.model('SensorData');

module.exports.profileRead = (req, res) => {
  // If no user ID exists in the JWT return a 401
  if (!req.payload._id) {
    res.status(401).json({
      message: 'UnauthorizedError: private profile'
    });
  } else {
    // Otherwise continue
    /*console.log("-----------------------");
    console.log(req.payload);
    console.log("-----------------------");*/
    userId = req.payload._id;
    User.findById(userId).exec(function(err, user) {

      //we need to return, client name, user info, and latest tanks info
      result = "{ \"user\" : "+ JSON.stringify(user);

      //var topics = [];
      Client.findOne({ users: userId }, function(err, client_) {

        if(err){
          console.log("message: ",err);
          return res.status(500).send(err);
        }
        //console.log(client_);
        if(client_ !== null){

          // we should check role to see if it has tanksystem or not!
          Role.find({ "_id" : { "$in" : user.roles } }, function(err, allroles) { 

            //console.log(allroles);

            doesItHavTanks = allroles.filter(function (entry) { return entry["name"] === "admin_tanks" || entry["name"] === "user_tanks" || entry["name"] === "admin_tanks_nocontrol" || entry["name"] === "user_tanks_nocontrol"});
            
            if ( doesItHavTanks.length == 0 ) {

              result += ", \"client\" :  "+JSON.stringify(client_);
                  result += " }";
              console.log(result);
              var resultJSON = JSON.parse(result);
              return res.status(200).send(resultJSON);

            } else {

              TanksSystem.findOne({ client : client_._id }).populate( "tanks" ).exec( function (err1, tanksSystems) {

                if(err1){
                    //console.log("message: ",err1);
                    return res.status(500).send(err1);
                }

                MonitorTanksSystem.findOne({ client : client_._id }).populate( "monitortanks" ).exec( function (error2, monitortanksSystems) {

                if(error2){
                    //console.log("message: ",err1);
                    return res.status(500).send(error2);
                }                

                  WineryBarrelStorage.findOne({ client : client_._id }).populate( "sensors" ).exec( function (err11, winerySystems){

                    if(err11){
                      console.log("message: ",err1, err11);
                      return res.status(500).send("ERROR!!!");
                    }

                    result += ", \"client\" :  "+JSON.stringify(client_);

                    //console.log(tanksSystems);
                    // topic standard ixenbc/alximia/json/1/
                    //falta for para todos los sistemas de tanques
                    if(tanksSystems != null){
                      result+=" , \"tanksSystem\" : "
                                +JSON.stringify(tanksSystems)+" , \"tanks\" : " + JSON.stringify(tanksSystems.tanks);
                      
                    }
                    if(monitortanksSystems != null){
                      result+=" , \"monitorTanksSystem\" : "
                                +JSON.stringify(monitortanksSystems)+" , \"monitortanks\" : " + JSON.stringify(monitortanksSystems.monitortanks);
                      
                    }
                    if(winerySystems != null){
                      result +=" , \"barrelStorage\" : " + JSON.stringify(winerySystems) +" , \"sensors\" : " + JSON.stringify(winerySystems.sensors) ;
                    }
                    result += " }";
                    console.log(result);
                    var resultJSON = JSON.parse(result);
                    return res.status(200).send(resultJSON);

                  });
                  
                });

              });

            }

          });

        } else {
          // no client!
          result += " }";
          var resultJSON = JSON.parse(result);
          return res.status(200).send(resultJSON);
        }

      });

      /*

      Client.find({ users: req.payload._id }, function(err, client) {

        TanksSystem.find({ client: client._id }, function(err, tankssyst) {
          if (err) {
            res.send(err);
          } else {
            //res1.send(result);
            //result += " \"TanksSystem\" : "+ JSON.stringify(res1)+" }";
            if(tankssyst.tanks){
              
              result += ", { \"tanksSystem\" : \""+tankssyst.name+"\"  , \"tanks\" : [ ";

              for (var i = 0; i < tankssyst.tanks.length; i++) {
                Tank.findById(tankssyst.tanks[i]._id, function(err, tank) {
                  if (err) {
                    res.send(err);
                  } else {
                    result += JSON.stringify(tank)+" , ";
                  }
                });
                result += " ] }";
              }

            }
            
          }
        });

      });
      */
      //result+=" }";

    });

    /*
     * Naturally, this should be fleshed out with some more error trapping — 
     * for example, if the user isn’t found — but this snippet is kept brief 
     * to demonstrate the key points of the approach.
     */
  }
};

module.exports.isAdmin = (req, res, next) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    Role.find(
      {
        _id: { $in: user.roles }
      },
      (err, roles) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        for (let i = 0; i < roles.length; i++) {
          if (roles[i].name === "admin") {
            next();
            return;
          }
        }

        res.status(403).send({ message: "Require Admin Role!" });
        return;
      }
    );
  });
};

module.exports.isOperator = (req, res, next) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    Role.find(
      {
        _id: { $in: user.roles }
      },
      (err, roles) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        for (let i = 0; i < roles.length; i++) {
          if (roles[i].name === "operator") {
            next();
            return;
          }
        }

        res.status(403).send({ message: "Require Operator Role!" });
        return;
      }
    );
  });
};