const mongoose = require('mongoose');
const MonitorTank = mongoose.model('MonitorTank');
const ObjectId = mongoose.Types.ObjectId;

//function getAll(req, res, next)
module.exports.getAll = (req, res, next) => {
  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks_nocontrol" || entry["name"] === "user_tanks_nocontrol" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
    MonitorTank.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
  }
}

module.exports.getAllByClient = (req, res, next) => {
  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks_nocontrol" || entry["name"] === "user_tanks_nocontrol" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
    MonitorTank.find({ client: req.params.id })
        .then(users => res.json(users))
        .catch(err => next(err));
  }
}

//function getCurrent(req, res, next) 
/*module.exports.getCurrent = (req, res, next) => {
    User.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}*/

//function getById(req, res, next) {
module.exports.getById = (req, res, next) => {
  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks_nocontrol" || entry["name"] === "user_tanks_nocontrol" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
    MonitorTank.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
  }
}

//function update(req, res, next) {
module.exports.update = (req, res, next) => {
  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks_nocontrol" || entry["name"] === "user_tanks_nocontrol" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
    MonitorTank.updateOne({ _id : req.params.id}, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
  }
}

module.exports.updateEmptyStatus = (req, res, next) => {
  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks_nocontrol" || entry["name"] === "user_tanks_nocontrol" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
    //console.log(req.params.id);
    //console.log(req.body);
    MonitorTank.updateOne({ _id : req.params.id}, { $set: { isEmpty: req.body.data } })
        .then(() => res.json({}))
        .catch(err => next(err));
  }
}


//function _delete(req, res, next) {
module.exports._delete = (req, res, next) => {
  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks_nocontrol" || entry["name"] === "super_admin"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {
    MonitorTank.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
  }
}


module.exports.register = (req, res) => {
  const currentUser = req.payload;
  // only allow admins to access other user records
  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "super_admin" || entry["name"] === "admin_tanks_nocontrol"});
  if ( isItAdmin.length == 0 ) {
      return res.status(401).send({ message: 'Unauthorized' });
  } else {

    const tank = new MonitorTank();

    if(req.body.name){

      tank.name = req.body.name;
      if(req.body.volume){tank.volume = req.body.volume;}
      if(req.body.client){tank.client = req.body.client;}
      if(req.body.varietal){tank.varietal = req.body.varietal; }
      if(req.body.type){tank.type = req.body.type;}  
      if(req.body.observaciones){tank.observations = req.body.observaciones;}   
      tank.isEmpty = false; 
      /*
        Don’t forget that, in reality, this code would have a number of error traps, 
        validating form inputs and catching errors in the save function. 
        They’re omitted here to highlight the main functionality of the code, 
        but if you’d like a refresher, check out “Forms, File Uploads and Security with Node.js and Express”.
        https://www.sitepoint.com/forms-file-uploads-security-node-express/
       */
      tank.save((err, tank) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        
        res.status(200);
        res.send({ message: "MonitorTank was registered successfully!" });

      });

    } else {
      // No values to register user
      res.status(401).json(info);
    }

  }
  
};