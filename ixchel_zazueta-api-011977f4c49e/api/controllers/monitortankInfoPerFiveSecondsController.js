const mongoose = require('mongoose');
const MonitorTankInfoPerFiveSeconds = mongoose.model('MonitorTankInfoPerFiveSeconds');
const LatestMonitorTankInfo = mongoose.model('LatestMonitorTankInfo');
const MonitorTanksSystem = mongoose.model('MonitorTanksSystem');
const Voltage = mongoose.model('Voltage');
const MonitorTank = mongoose.model('MonitorTank');
const Client = mongoose.model('Client');
const MonitorUserLog = mongoose.model('MonitorUserLog');
const Environment = mongoose.model('Environment');
const ObjectId = mongoose.Types.ObjectId;
const { Parser, transforms: { unwind } } = require('json2csv');

module.exports.getLatest = (req, res, next) => {
  if (!req.payload._id) {
  	res.status(401).json({
      message: 'UnauthorizedError: private profile'
    });
  } else {		
	const currentUser = req.payload;
	// only allow admins to access other user records
	isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks_nocontrol" || entry["name"] === "user_tanks_nocontrol" });
	if ( isItAdmin.length == 0 ) {
	    return res.status(401).send({ message: 'Unauthorized' });
	} else {
	    // Otherwise continue
		LatestMonitorTankInfo.findOne({ monitortank: req.params.id }).sort('-createdDate')
		  	.then(
		  		tankInfo => { //tankInfo ? res.json(tankInfo) : res.sendStatus(404)
			  		if(tankInfo){
			  			MonitorTank.findOne({ _id : req.params.id }).then(
			  				tanktankinfo => {
			  					if(tanktankinfo){
				  					tankInfo.maxTemp = tanktankinfo.maxTemp;
				  					tankInfo.minTemp = tanktankinfo.minTemp;
				  					res.json(tankInfo);
			  					} else {
			  						res.sendStatus(404);
			  					}
			  				}
			  			).catch(err => next(err));
		  			} else {
		  				res.sendStatus(404);
		  			}

		  		}	
		  	).catch(err => next(err));
	}
  }
}

module.exports.getReport = (req, res, next) => {
  if (!req.payload._id) {
  	res.status(401).json({
      message: 'UnauthorizedError: private profile'
    });
  } else {
	  // Otherwise continue
	  const currentUser = req.payload;
	  // only allow admins to access other user records
	  isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks_nocontrol" || entry["name"] === "user_tanks_nocontrol" });
	  if ( isItAdmin.length == 0 ) {
	      return res.status(401).send({ message: 'Unauthorized' });
	  } else {  

	  if(req.params.startDate && req.params.endDate ){
		
		lngtSteps = 60/req.params.stepMins;
		arrSteps = [];
		for (var i = 0; i < lngtSteps; i++) {
			stp = req.params.stepMins*i;
			arrSteps.push(stp);
		}
		
		//console.log(new Date(req.params.endDate));

		MonitorTankInfoPerFiveSeconds.aggregate([
	        { $match: 
	        	{
	        		$and: [
	        		{ monitortank : ObjectId(req.params.id) },
	        		//{ minute: { "$in" : arrSteps }},
	        		//{ second: { $gt: 0 }},
	        		//{ second: { $lt: 6 }},
	        		{ createdDate: {
					    $gte: new Date(req.params.endDate)
				      } 
				  	},
				  	{ createdDate: {
					    $lt: new Date(req.params.startDate)
				      } 
				  	}
	       			]
	    		}
	    	},
	    	{
			    $group: {
			        _id: { 
			        	  createdDate : {
			        	  "$toDate": {
			        	  //"$add": [
			        		 //{
					              "$subtract": [
					                  { "$toLong": "$createdDate"  },
					                  { "$mod": [ { "$toLong":  "$createdDate" }, 1000 * 60 * req.params.stepMins ] }
					              ]
					          }}//,
					          //{ $multiply : [ {$hour:"$createdDate"} , 3600*1000 ]}
					     //]
					 	//}
					    //interval: { $trunc: { $divide: [ {$minute:"$createdDate"}, Number(req.params.stepMins) ]}}
			        },
			        temp: { $avg: "$temp" },
			        //createdDate : _id,
			        /*
			        maxTemp: { $avg: "$maxTemp" },
					minTemp: { $avg: "$minTemp" }
					*/
			    }
			},
			{
				$sort: { _id : 1 }
			}
	    	
	    ])
		.exec()
		.then(
			tankInfo => {

			if(tankInfo){
				if(req.params.userLogs == "true"){
					MonitorUserLog.find({monitortank:req.params.id}).exec(function(err, userlogs) {
						res.json({ timeSeries : tankInfo , userLogs : userlogs});
					});
				} else {
					res.json({ timeSeries : tankInfo});
				}
			} 
			else {
				res.sendStatus(404);
			}

		  },
		  err => next(err)
	    );
	  }
	}
  }
}


module.exports.getAllReport = (req, res, next) => {
  if (!req.payload._id) {
  	res.status(401).json({
      message: 'UnauthorizedError: private profile'
    });
  } else {
	// Otherwise continue
	const currentUser = req.payload;
	// only allow admins to access other user records
	isItAdmin = currentUser.roles.filter(function (entry) { return entry["name"] === "admin_tanks_nocontrol" || entry["name"] === "user_tanks_nocontrol" });
	if ( isItAdmin.length == 0 ) {
	    return res.status(401).send({ message: 'Unauthorized' });
	} else {  

	  if(req.params.id){
		
	  	MonitorTankInfoPerFiveSeconds.find({ monitortank: req.params.id }, 'monitortank createdDate temp maxTemp minTemp')
		.sort({ createdDate: 1 })
		.then(
		  tankInfo => {

			if(tankInfo){
				//res.json({ timeSeries : tankInfo });
				const fields = ['createdDate', 'temp', 'maxTemp', 'minTemp'];
		        //const transforms = [unwind({ paths: ['user', 'user.name'] })];

		        const json2csvParser = new Parser({ fields });
		        const csv = json2csvParser.parse(tankInfo);
		        //console.log(csv);
		        res.setHeader('Content-disposition', 'attachment; filename=data.csv');
		        res.set('Content-Type', 'text/csv');
		        res.status(200).send(csv);
			} 
			else {
				res.sendStatus(404);
			}

		  },
		  err => next(err)
			
		).catch(err => next(err));

	  }
	}
  }
  
}
