const ctrlAuth = require('../controllers/authentication');
const ctrlProfile = require('../controllers/profile');
const ctrlUsers = require('../controllers/usersController');
const ctrlClients = require('../controllers/clientController');
const ctrlTanks = require('../controllers/tanksController');
const ctrlTanksSystem = require('../controllers/tanksSystemsController');
const ctrlTankInfo = require('../controllers/tankInfoPerFiveSecondsController');
const ctrlVoltage = require('../controllers/voltageController');
const ctrlSensors = require('../controllers/sensorsController');
const ctrlSensorData = require('../controllers/sensorDataController');
const ctrlWBS = require('../controllers/wineryBarrelStorageController');
const ctrlMonitorTanks = require('../controllers/monitortanksController');
const ctrlMonitorTanksSystem = require('../controllers/monitortanksSystemsController');
const ctrlMonitorTankInfo = require('../controllers/monitortankInfoPerFiveSecondsController');

const express = require('express');
const jwt = require('express-jwt');
const router = express.Router();
const multer = require('multer');


const config = require('../../config');

const auth = jwt({
  secret: config.jwtSecretKey,
  userProperty: 'payload',
  algorithms: ["HS256"]
});

// profile
router.get('/profile', auth, ctrlProfile.profileRead);
router.get('/latest_tank_info/:id', auth, ctrlTankInfo.getLatest);
router.post('/change_control_status_tank/:id', auth, ctrlTankInfo.changeControlStatus);
router.post('/change_temperature_ranges_tank/:id', auth, ctrlTankInfo.changeControlTemperature);
router.post('/log_message/:id', auth, ctrlUsers.registerLog);
router.get('/log_message/:id/:typeMsg', auth, ctrlUsers.getAllUserLogs);
router.get('/log_message/:id/:startDate/:endDate/:typeMsg', auth, ctrlUsers.getUserLogs);
router.get('/tank_time_series/:id/:startDate/:endDate/:stepMins/:userLogs', auth, ctrlTankInfo.getReport);
router.get('/tank_time_series_all/:id', auth, ctrlTankInfo.getAllReport);
router.get('/voltage_series/:id/:startDate/:endDate/:stepMins', auth, ctrlVoltage.getReport);
router.get('/voltage_series_all/:id', auth, ctrlVoltage.getAllReport);
router.get('/environment_time_series/:id/:startDate/:endDate/:stepMins', auth, ctrlTankInfo.getEnvironmentReport);
router.put('/change_empty_status_tank/:id', auth, ctrlTanks.updateEmptyStatus);

router.put('/change_empty_status_monitor_tank/:id', auth, ctrlMonitorTanks.updateEmptyStatus);

router.get('/latest_monitortank_info/:id', auth, ctrlMonitorTankInfo.getLatest);
router.get('/monitortank_time_series/:id/:startDate/:endDate/:stepMins/:userLogs', auth, ctrlMonitorTankInfo.getReport);
router.get('/monitortank_time_series_all/:id', auth, ctrlMonitorTankInfo.getAllReport);

router.post('/log_monitormessage/:id', auth, ctrlUsers.registerMonitorLog);
router.get('/log_monitormessage/:id/:typeMsg', auth, ctrlUsers.getAllMonitorUserLogs);
router.get('/log_monitormessage/:id/:startDate/:endDate/:typeMsg', auth, ctrlUsers.getMonitorUserLogs);

//winery stuff
router.get('/sensor_data_all/:id', auth, ctrlSensorData.getAllReport);
router.get('/sensor_data_humidity_all/:id', auth, ctrlSensorData.getAllHumReport);
router.get('/latest_sensor_info/:id', auth, ctrlSensorData.getLatest);
router.get('/sensors_data_report/:id/:startDate/:endDate/:stepMins', auth, ctrlSensorData.getReport);


// authentication
router.post('/register', auth, ctrlAuth.register);
router.post('/registerWithNewClient', auth, ctrlAuth.registerWClient);
router.post('/registerWithNoClient', ctrlAuth.registerWOPermWOClient);

router.post('/login', ctrlAuth.login);
router.put('/change_password/:id', auth, ctrlAuth.changePassword);

//CRUD
router.get('/users', auth, ctrlUsers.getAll);
router.get('/clients', auth, ctrlClients.getAll);
router.get('/users/:id', auth, ctrlUsers.getById);
router.put('/users/:id', auth, ctrlUsers.update);
router.delete('/users/:id', auth, ctrlUsers._delete);


//Client register
// Save file to server storage
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, './uploads/');
      //cb(null, '/home/users/ubuntu/ixchel/enotek-app/vintek-master/uploads/');
    },
    filename: (req, file, cb) => {
      console.log(file);
      var filetype = '';
      if(file.mimetype === 'image/gif') {
        filetype = 'gif';
      }
      if(file.mimetype === 'image/png') {
        filetype = 'png';
      }
      if(file.mimetype === 'image/jpeg') {
        filetype = 'jpg';
      }
      cb(null, 'image-' + Date.now() + '.' + filetype);
    }
});
var upload = multer({storage: storage});
router.post('/register_client',  upload.single('file'), ctrlClients.register);
router.put('/clients/:id',  upload.single('file'), ctrlClients.update);

router.post('/tanks', auth, ctrlTanks.register);
router.get('/tanks', auth, ctrlTanks.getAll);
router.get('/tanks/:id', auth, ctrlTanks.getById);
router.get('/all_tanks/:id', auth, ctrlTanks.getAllByClient);
router.put('/tanks/:id', auth, ctrlTanks.update);
router.delete('/tanks/:id', auth, ctrlTanks._delete);

router.post('/monitor_tanks', auth, ctrlMonitorTanks.register);
router.get('/monitor_tanks', auth, ctrlMonitorTanks.getAll);
router.get('/all_monitor_tanks/:id', auth, ctrlMonitorTanks.getAllByClient);
router.get('/monitor_tanks/:id', auth, ctrlMonitorTanks.getById);
router.put('/monitor_tanks/:id', auth, ctrlMonitorTanks.update);
router.delete('/monitor_tanks/:id', auth, ctrlMonitorTanks._delete);

router.post('/tanks_system', auth, ctrlTanksSystem.register);
router.get('/tanks_system', auth, ctrlTanksSystem.getAll);
router.get('/tanks_system/:id', auth, ctrlTanksSystem.getById);
router.put('/tanks_system/:id', auth, ctrlTanksSystem.update);
router.delete('/tanks_system/:id', auth, ctrlTanksSystem._delete);

router.post('/monitor_tanks_system', auth, ctrlMonitorTanksSystem.register);
router.get('/monitor_tanks_system', auth, ctrlMonitorTanksSystem.getAll);
router.get('/monitor_tanks_system/:id', auth, ctrlMonitorTanksSystem.getById);
router.put('/monitor_tanks_system/:id', auth, ctrlMonitorTanksSystem.update);
router.delete('/monitor_tanks_system/:id', auth, ctrlMonitorTanksSystem._delete);

router.post('/sensors', auth, ctrlSensors.register);
router.get('/sensors', auth, ctrlSensors.getAll);
router.get('/sensors/:id', auth, ctrlSensors.getById);
router.put('/sensors/:id', auth, ctrlSensors.update);
router.delete('/sensors/:id', auth, ctrlSensors._delete);


router.post('/winery_barrel_storage', ctrlWBS.register);
router.get('/winery_barrel_storage', ctrlWBS.getAll);
router.get('/winery_barrel_storage/:id', ctrlWBS.getById);
router.put('/winery_barrel_storage/:id', ctrlWBS.update);
router.delete('/winery_barrel_storage/:id', ctrlWBS._delete);

module.exports = router;