const jwtSecretKey = process.env.JWT_SECRET_KEY;
//const serverPort = process.env.PORT || 8000; // 8000 is the default value in case if the env variable has not been set

module.exports = {
   jwtSecretKey: jwtSecretKey
   //serverPort: serverPort
}